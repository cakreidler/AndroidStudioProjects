package com.example.chris.database2;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.speech.tts.TextToSpeech;
import android.database.Cursor;
import java.util.Locale;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private SQLiteAdapter muhbrands;
    public View contentMain;

    int red;
    int green;
    int orange;
    int yellow;
    int purple;
    int pink;
    int tan;
    int white;
    int bgcolor;

    TextToSpeech t1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView BrandList = (ListView)findViewById(R.id.list_view);
        contentMain = (View) findViewById(R.id.activity_main);

        red = ContextCompat.getColor(MainActivity.this, R.color.Red);
        green = ContextCompat.getColor(MainActivity.this, R.color.Green);
        orange = ContextCompat.getColor(MainActivity.this, R.color.Orange);
        yellow = ContextCompat.getColor(MainActivity.this, R.color.Yellow);
        purple = ContextCompat.getColor(MainActivity.this, R.color.Purple);
        pink = ContextCompat.getColor(MainActivity.this, R.color.Pink);
        tan = ContextCompat.getColor(MainActivity.this, R.color.Tan);
        white = ContextCompat.getColor(MainActivity.this, R.color.White);

        t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.US);
                }
            }
        });

        muhbrands = new SQLiteAdapter(this);
        muhbrands.openToWrite();
        muhbrands.deleteAll();
        muhbrands.insert("Cola", "Red");
        muhbrands.insert("Kountry Mist", "Green");
        muhbrands.insert("Lemon", "Yellow");
        muhbrands.insert("Grape", "Purple");
        muhbrands.insert("Red Alert", "Red");
        muhbrands.insert("Orange Smash", "Orange");
        muhbrands.insert("Green Apple", "Green");
        muhbrands.insert("Vanilla Cola", "Tan");
        muhbrands.insert("Tropical Punch", "Pink");
        muhbrands.close();
        muhbrands = new SQLiteAdapter(this);
        muhbrands.openToRead();

        Cursor cursor = muhbrands.queueAll();
        //I know this is depreciated but this is the only way I could find to do this easily
        startManagingCursor(cursor);

        String[] from = new String[]{SQLiteAdapter.KEY_CONTENT, SQLiteAdapter.KEY_CONTENT2};
        int[] to = new int[]{R.id.brand, R.id.color};

        SimpleCursorAdapter cursorAdapter = new SimpleCursorAdapter(this, R.layout.brandview, cursor, from, to);

        BrandList.setAdapter(cursorAdapter);

        muhbrands.close();

        BrandList.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                TextView colorTV = (TextView) view.findViewById(R.id.color);
                String colors = colorTV.getText().toString();

                if (colors.equals("Red"))
                {
                    contentMain.setBackgroundColor(red);
                    //.speak is depreciated but only because the receent version doesnt work with my build's API.
                    bgcolor = red;
                    t1.speak(colors, TextToSpeech.QUEUE_FLUSH, null);
                }
                if (colors.equals("Green"))
                {
                    contentMain.setBackgroundColor(green);
                    t1.speak(colors, TextToSpeech.QUEUE_FLUSH, null);
                    bgcolor = green;
                }
                if (colors.equals("Yellow"))
                {
                    t1.speak(colors, TextToSpeech.QUEUE_FLUSH, null);
                    contentMain.setBackgroundColor(yellow);
                    bgcolor = yellow;
                }
                if (colors.equals("Purple"))
                {
                    contentMain.setBackgroundColor(purple);
                    t1.speak(colors, TextToSpeech.QUEUE_FLUSH, null);
                    bgcolor = purple;
                }
                if (colors.equals("Orange"))
                {
                    contentMain.setBackgroundColor(orange);
                    t1.speak(colors, TextToSpeech.QUEUE_FLUSH, null);
                    bgcolor = orange;
                }
                if (colors.equals("Pink"))
                {
                    contentMain.setBackgroundColor(pink);
                    t1.speak(colors, TextToSpeech.QUEUE_FLUSH, null);
                    bgcolor = pink;
                }
                if (colors.equals("Tan"))
                {
                    contentMain.setBackgroundColor(tan);
                    t1.speak(colors, TextToSpeech.QUEUE_FLUSH, null);
                    bgcolor = tan;
                }
            }
        });
    }
    @Override
    public void onRestoreInstanceState(Bundle inState)
    {
        super.onRestoreInstanceState(inState);
        bgcolor = inState.getInt("bgcolor");
        contentMain.setBackgroundColor(bgcolor);
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        outState.putInt("bgcolor", bgcolor);
    }
}
