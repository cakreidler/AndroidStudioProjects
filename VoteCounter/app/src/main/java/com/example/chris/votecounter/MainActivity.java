package com.example.chris.votecounter;

import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button dcounter;
    private Button rcounter;
    private Button ccounter;
    public int Dcount;
    public int Rcount;
    private TextView Dtext;
    private TextView Rtext;
    private String dcopytext = "0";
    private String rcopytext = "0";
    int donkeyBgColor;
    int elephantBgColor;
    int tieBgColor;
    public View contentMain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dcounter = (Button) findViewById(R.id.dbutton);
        rcounter = (Button) findViewById(R.id.Rbutton);
        ccounter = (Button) findViewById(R.id.cbutton);
        Dtext = (TextView) findViewById(R.id.Dnum);
        Rtext = (TextView) findViewById(R.id.Rnum);
        contentMain = (View) findViewById(R.id.activity_main);

        final int donkeyBgColor = ContextCompat.getColor(MainActivity.this, R.color.donkeyBgColor);
        final int elephantBgColor = ContextCompat.getColor(MainActivity.this, R.color.elephantBgColor);
        final int tieBgColor = ContextCompat.getColor(MainActivity.this, R.color.tieBgColor);

        dcounter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                Dcount++;
                dcopytext = Integer.toString(Dcount);
                Dtext.setText(dcopytext);
                if (Dcount == Rcount)
                {
                    contentMain.setBackgroundColor(tieBgColor);
                }
                else if(Dcount > Rcount)
                {
                    contentMain.setBackgroundColor(donkeyBgColor);
                }

            }
        });

        rcounter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                Rcount++;
                rcopytext = Integer.toString(Rcount);
                Rtext.setText(rcopytext);
                if (Rcount == Dcount)
                {
                    contentMain.setBackgroundColor(tieBgColor);
                }
                else if(Rcount > Dcount)
                {
                    contentMain.setBackgroundColor(elephantBgColor);
                }

            }
        });

        ccounter.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                Rcount = 0;
                rcopytext = Integer.toString(Rcount);
                Rtext.setText(rcopytext);
                Dcount = 0;
                dcopytext = Integer.toString(Dcount);
                Dtext.setText(dcopytext);
                contentMain.setBackgroundColor(tieBgColor);
            }
        });
    }


}
