package com.example.chris.calculator;

/**
 * Created by Chris on 2/16/2017.
 */

public enum Operation {
    ADD, SUBTRACT, MULTIPLY, DIVIDE, EQUALS, CLEAR, CLEAR_ENTRY;
}
