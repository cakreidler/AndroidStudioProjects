package com.example.chris.calculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


import static com.example.chris.calculator.Operation.ADD;
import static com.example.chris.calculator.Operation.CLEAR;
import static com.example.chris.calculator.Operation.CLEAR_ENTRY;
import static com.example.chris.calculator.Operation.DIVIDE;
import static com.example.chris.calculator.Operation.EQUALS;
import static com.example.chris.calculator.Operation.MULTIPLY;
import static com.example.chris.calculator.Operation.SUBTRACT;

public class MainActivity extends AppCompatActivity {

    private TextView Dtext;

    private CalculatorEngine ce = new CalculatorEngine(15);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Dtext = (TextView) findViewById(R.id.Dtext);

        //Here begin the bindings for the buttons.

        Button num0Button = (Button) findViewById(R.id.num0Button);
        num0Button.setOnClickListener(new View.OnClickListener()
        {


            public void onClick(View v)
         {
             ce.insert('0');
             Dtext.setText(ce.getDisplay());
         }
        });

        Button num1Button = (Button) findViewById(R.id.num1Button);
        num1Button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                ce.insert('1');
                Dtext.setText(ce.getDisplay());
            }
        });

        Button num2Button = (Button) findViewById(R.id.num2Button);
        num2Button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                ce.insert('2');
                Dtext.setText(ce.getDisplay());
            }
        });

        Button num3Button = (Button) findViewById(R.id.num3Button);
        num3Button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                ce.insert('3');
                Dtext.setText(ce.getDisplay());
            }
        });

        Button num4Button = (Button) findViewById(R.id.num4Button);
        num4Button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                ce.insert('4');
                Dtext.setText(ce.getDisplay());
            }
        });

        Button num5Button = (Button) findViewById(R.id.num5Button);
        num5Button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                ce.insert('5');
                Dtext.setText(ce.getDisplay());
            }
        });

        Button num6Button = (Button) findViewById(R.id.num6Button);
        num6Button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                ce.insert('6');
                Dtext.setText(ce.getDisplay());
            }
        });

        Button num7Button = (Button) findViewById(R.id.num7Button);
        num7Button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                ce.insert('7');
                Dtext.setText(ce.getDisplay());
            }
        });

        Button num8Button = (Button) findViewById(R.id.num8Button);
        num8Button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                ce.insert('8');
                Dtext.setText(ce.getDisplay());
            }
        });

        Button num9Button = (Button) findViewById(R.id.num9Button);
        num9Button.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                ce.insert('9');
                Dtext.setText(ce.getDisplay());
            }
        });

        Button PButton = (Button) findViewById(R.id.PButton);
        PButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                ce.perform(ADD);
            }
        });

        Button EButton = (Button) findViewById(R.id.EButton);
        EButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                ce.perform(EQUALS);
                Dtext.setText(ce.getDisplay());
            }
        });

        Button MButton = (Button) findViewById(R.id.MButton);
        MButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                ce.perform(MULTIPLY);
            }
        });

        Button DButton = (Button) findViewById(R.id.DButton);
        DButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                ce.perform(DIVIDE);
            }
        });

        Button SButton = (Button) findViewById(R.id.SButton);
        SButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                ce.perform(SUBTRACT);
            }
        });

        Button pnButton = (Button) findViewById(R.id.pnButton);
        pnButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                ce.toggleSign();
                Dtext.setText(ce.getDisplay());
            }
        });

        Button CEButton = (Button) findViewById(R.id.CEButton);
        CEButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                ce.perform(CLEAR_ENTRY);
                Dtext.setText(ce.getDisplay());
            }
        });

        Button CButton = (Button) findViewById(R.id.CButton);
        CButton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View v)
            {
                ce.perform(CLEAR);
                Dtext.setText(ce.getDisplay());
            }
        });


    }

    ///AHAHAHAHAHA IT WORKS!!!!!
    //Saving the instance state on rotation.

    @Override
   public void onRestoreInstanceState(Bundle inState)
    {
        super.onRestoreInstanceState(inState);
        ce = (CalculatorEngine) inState.getSerializable("ce");
        Dtext.setText(ce.getDisplay());

    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        outState.putSerializable("ce", ce);

        super.onSaveInstanceState(outState);
    }
}


