package com.example.chris.stockquotes;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.EditText;
import android.util.Log;
import android.view.View;
import android.view.KeyEvent;
import android.widget.Toast;
import java.io.IOException;

public class MainActivity extends AppCompatActivity
{
    private TextView symbol;
    private TextView name;
    private TextView LTradePrice;
    private TextView LTradeTime;
    private TextView change;
    private TextView FTWrange;
    private String symbolst;
    private String namest;
    private String LTradePricest;
    private String LTradeTimest;
    private String changest;
    private String FTWrangest;
    private String sym;
    private EditText editText;
    private Stock stk = new Stock("");

    public class Wrapper
    {
        public Stock wstk;
        public boolean fstate;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        symbol = (TextView) findViewById(R.id.Esymbol);
        name = (TextView) findViewById(R.id.Ename);
        LTradePrice = (TextView) findViewById(R.id.ELTradePrice);
        LTradeTime = (TextView) findViewById(R.id.ELTradeTime);
        change = (TextView) findViewById(R.id.EChange);
        FTWrange = (TextView) findViewById(R.id.EFTWrange);

        editText = (EditText) findViewById(R.id.editText);
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE)
                {
                    sym = editText.getText().toString();
                    Stockgetta stkget = new Stockgetta();
                    stkget.execute(sym);

                    handled = true;
                }
                return handled;
            }

        });
    }
    private class Stockgetta extends AsyncTask<String, Void, Wrapper>
    {

        protected Wrapper doInBackground(String... sym)
        {
            String path = "I'm walkin' here";
            stk = new Stock(sym[0]);
            boolean failure;

            try {
                stk.load();
                failure = false;
            }
            catch (StringIndexOutOfBoundsException e)
            {
                e.printStackTrace();
                failure = true;
                Log.d("Index", path);
            }
            catch (IOException e)
            {
                e.printStackTrace();
                failure = true;
                Log.d("IOE", path);
            }

            Wrapper w = new Wrapper();
            w.wstk = stk;
            w.fstate = failure;
            return w;
        }

        protected void onPostExecute(Wrapper w)
        {
            String logm;
            if(w.fstate == true)
            {
                logm = "WHY AM I HERE!!";
                Log.d("OH MY GOD",logm);
                symbol.setText("");
                change.setText("");
                LTradePrice.setText("");
                LTradeTime.setText("");
                name.setText("");
                FTWrange.setText("");
            }
            else if(w.fstate == false)
            {
                logm = "IT WERKS";
                Log.d("HURR DURR", logm);
                textsetta(w.wstk);
            }
        }
    }
    public void textsetta(Stock stk)
    {
        symbol.setText(stk.getSymbol());
        change.setText(stk.getChange());
        LTradePrice.setText(stk.getLastTradePrice());
        LTradeTime.setText(stk.getLastTradeTime());
        name.setText(stk.getName());
        FTWrange.setText(stk.getRange());
    }

    @Override
    public void onRestoreInstanceState(Bundle inState)
    {
        super.onRestoreInstanceState(inState);
        symbolst = inState.getString("symbol");
        changest = inState.getString("change");
        LTradePricest = inState.getString("LTP");
        LTradeTimest = inState.getString("LTT");
        namest = inState.getString("name");
        FTWrangest = inState.getString("range");
        symbol.setText(symbolst);
        change.setText(changest);
        LTradePrice.setText(LTradePricest);
        LTradeTime.setText(LTradeTimest);
        name.setText(namest);
        FTWrange.setText(FTWrangest);
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        symbolst = stk.getSymbol();
        changest = stk.getChange();
        LTradePricest = stk.getLastTradePrice();
        LTradeTimest = stk.getLastTradeTime();
        namest = stk.getName();
        FTWrangest = stk.getRange();
        outState.putString("symbol", symbolst);
        outState.putString("change", changest);
        outState.putString("LTP", LTradePricest);
        outState.putString("LTT", LTradeTimest);
        outState.putString("name", namest);
        outState.putString("range", FTWrangest);
        super.onSaveInstanceState(outState);
    }
}

