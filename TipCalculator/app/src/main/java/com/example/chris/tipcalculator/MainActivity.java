package com.example.chris.tipcalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.view.KeyEvent;

public class MainActivity extends AppCompatActivity {

    private double bill;
    private String thang;
    private double tipp;
    private double tip;
    private double total;
    private TextView Tipt;
    private TextView Totalt;
    private EditText editText;
    //private RadioGroup radiogroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Tipt = (TextView) findViewById(R.id.textview4);
        Totalt = (TextView) findViewById(R.id.textview5);
        RadioGroup rg = (RadioGroup) findViewById(R.id.radiogroup1);

        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {

            //RadioGroup radiogroup = (RadioGroup) findViewById(R.id.radiogroup1);

            @Override
            public void onCheckedChanged(RadioGroup group, int Checkid)
            {
                //boolean checked = ((RadioButton) view).isChecked();

                switch (Checkid)
                {
                    case R.id.radioButton7:
                        //if (checked)
                            setTipPercent(0.10);
                        break;
                    case R.id.radioButton8:
                        //if (checked)
                            setTipPercent(0.15);
                        break;
                    case R.id.radioButton9:
                        //if (checked)
                            setTipPercent(0.18);
                        break;
                    case R.id.radioButton10:
                        //if (checked)
                            setTipPercent(0.20);
                        break;
                }

            }
        });

        editText = (EditText) findViewById(R.id.editText);
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE)
                {
                    thang = editText.getText().toString();
                    bill = Double.parseDouble(thang);
                    Log.d("Billval", thang);
                    handled = true;
                }
                return handled;
            }
        });



    }

    public void setTipPercent(double tipquer) {
        String thang;
        tipp = tipquer;
        tip = round(bill * tipp, 2);
        thang = String.valueOf(tip);
        Log.d("Billval", thang);
        total = round(bill + (bill * tipp), 2);
        thang = String.valueOf(total);
        Log.d("Billval", thang);
        Tipt.setText(Double.toString(tip));
        Totalt.setText(Double.toString(total));
    }

    @Override
    public void onRestoreInstanceState(Bundle inState) {
        super.onRestoreInstanceState(inState);
        tip = inState.getDouble("tip");
        total = inState.getDouble("total");
        Tipt.setText(Double.toString(tip));
        Totalt.setText(Double.toString(total));
        bill = inState.getDouble("bill");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putDouble("tip", tip);
        outState.putDouble("total", total);
        outState.putDouble("bill", bill);
        super.onSaveInstanceState(outState);
        }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

}

