package com.example.chris.database;

/**
 * Created by Chris on 4/6/2017.
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class DBManager {

    private SQLitehelper dbHelper;

    private Context context;

    private SQLiteDatabase database;

    public DBManager(Context c) {
        context = c;
    }

    public DBManager open() throws SQLException {
        dbHelper = new SQLitehelper(context);
        database = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbHelper.close();
    }

    public void insert(String name, String desc) {
        ContentValues contentValue = new ContentValues();
        contentValue.put(SQLitehelper.KEY_BRANDS_FLAVOR, name);
        contentValue.put(SQLitehelper.KEY_BRANDS_COLOR, desc);
        database.insert(SQLitehelper.TABLE_BRANDS, null, contentValue);
    }

    public Cursor fetch() {
        String[] columns = new String[] { SQLitehelper.KEY_BRANDS_ID, SQLitehelper.KEY_BRANDS_FLAVOR, SQLitehelper.KEY_BRANDS_COLOR };
        Cursor cursor = database.query(SQLitehelper.TABLE_BRANDS, columns, null, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return cursor;
    }

    public int update(long _id, String name, String desc) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(SQLitehelper.KEY_BRANDS_FLAVOR, name);
        contentValues.put(SQLitehelper.KEY_BRANDS_COLOR, desc);
        int i = database.update(SQLitehelper.TABLE_BRANDS, contentValues, SQLitehelper.KEY_BRANDS_ID + " = " + _id, null);
        return i;
    }
}
