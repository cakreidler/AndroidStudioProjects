package com.example.chris.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

/**
 * Created by Chris on 4/2/2017.
 */

public class SQLitehelper extends SQLiteOpenHelper
{
    static final int DATABASE_VERSION = 1;
     static final String DATABASE_NAME = "brandsofChek.DB";
    public static final String TABLE_BRANDS = "Brands";
    public static final String KEY_BRANDS_ID = "id";
    public static final String KEY_BRANDS_FLAVOR = "flavor";
    public static final String KEY_BRANDS_COLOR = "color";



    //static final String DB_NAME = "CHEKSODABRANDS.DB";

    private static final String CREATE_TABLE = "create table " + TABLE_BRANDS + "(" + KEY_BRANDS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + KEY_BRANDS_FLAVOR + " TEXT NOT NULL, " + KEY_BRANDS_COLOR + " TEXT);";



    public SQLitehelper(Context context)
    {super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }
    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        // SQL for upgrading the tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BRANDS);
        onCreate(db);
    }


}
