package com.example.chris.phonecall;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{
    private Button cbutton;
    private EditText editText;
    private String pnumber = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText = (EditText) findViewById(R.id.editText);
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_DONE)
                {
                    pnumber = editText.getText().toString();
                    handled = true;
                }
                return handled;
            }
        });
        cbutton = (Button) findViewById(R.id.button);
        cbutton.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                Log.d("Index", pnumber);
                pnumber = editText.getText().toString();
                Log.d("Index", pnumber);
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + pnumber));

                if (ActivityCompat.checkSelfPermission(MainActivity.this,
                        Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(callIntent);
            }
        });
    }

    @Override
    public void onRestoreInstanceState(Bundle inState) {
        super.onRestoreInstanceState(inState);
        pnumber = inState.getString("pnumber");
        editText.setText(pnumber);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        pnumber = editText.getText().toString();
        outState.putString("pnumber", pnumber);
        super.onSaveInstanceState(outState);
    }
}
