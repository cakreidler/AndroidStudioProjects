package com.example.chris.budgetbuddy;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{
    SQLiteAdapter muhbudget;
    private TextView total;
    private int totalint;
    private String totalstr;
    private int curbudge;
    private String curbudgestr;
    private TextView budgetview;
    private SharedPreferences prefs;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        prefs = getSharedPreferences("muhprefs", Activity.MODE_PRIVATE);

        Button gotoaexpense = (Button) findViewById(R.id.toaexpenses);
        Button gotovexpense = (Button) findViewById(R.id.tovexpenses);
        Button gotocbudget = (Button) findViewById(R.id.tocbudget);
        total = (TextView) findViewById(R.id.totalview);
        budgetview = (TextView) findViewById(R.id.budgetview);
        muhbudget = new SQLiteAdapter(this);
        muhbudget.openToRead();
        totalint = muhbudget.summer();
        totalstr = Integer.toString(totalint);
        total.setText("$"+totalstr);
        muhbudget.close();
        curbudge = prefs.getInt("bugint", 0);
        curbudgestr = Integer.toString(curbudge);
        budgetview.setText("$"+curbudge);

        gotoaexpense.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                startaddexp();
            }
        });

        gotovexpense.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                startvexp();
            }
        });

        gotocbudget.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                startcbudget();
            }
        });

    }

    public void onResume()
    {
        super.onResume();
        muhbudget.openToRead();
        totalint = muhbudget.summer();
        totalstr = Integer.toString(totalint);
        total.setText("$"+totalstr);
        muhbudget.close();

        prefs = getSharedPreferences("muhprefs", Activity.MODE_PRIVATE);

        curbudge = prefs.getInt("bugint", 0);
        curbudgestr = Integer.toString(curbudge);
        budgetview.setText("$"+curbudge);
    }

    public void startaddexp ()
    {
        Intent sendutoaexpense = new Intent(this, javaaddexpenses.class);
        Log.e("EREWEGO","TOAEXP");

        startActivity(sendutoaexpense);
    }

    public void startvexp ()
    {
        Intent sendutovexpense = new Intent(this, javaexpenses.class);

        startActivity(sendutovexpense);
    }

    public void startcbudget ()
    {
        Intent sendutocbudget = new Intent(this, javacbudget.class);

        startActivity(sendutocbudget);
    }

}
