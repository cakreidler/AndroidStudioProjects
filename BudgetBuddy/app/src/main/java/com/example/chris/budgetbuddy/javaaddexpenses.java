package com.example.chris.budgetbuddy;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.app.Activity;

/**
 * Created by Chris on 4/29/2017.
 */

public class javaaddexpenses extends Activity
{
    private SQLiteAdapter muhexpense;
    private EditText expense;
    private EditText cost;
    private String expensestr;
    private String coststr;
    private Button goback;
    private Button addexpense;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        setContentView(R.layout.addexpenses);

        muhexpense = new SQLiteAdapter(this);
        muhexpense.openToWrite();

        cost = (EditText) findViewById(R.id.editcost);
        expense = (EditText) findViewById(R.id.edittype);
        goback = (Button) findViewById(R.id.gobackbutton);
        addexpense = (Button) findViewById(R.id.enterexpensebutton);



        goback.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View arg0)
            {
                muhexpense.close();
                finish();
            }
        });

        addexpense.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {


                coststr = cost.getText().toString();
                expensestr = expense.getText().toString();

                muhexpense.insert(expensestr, coststr);

            }
        });

    }
}
