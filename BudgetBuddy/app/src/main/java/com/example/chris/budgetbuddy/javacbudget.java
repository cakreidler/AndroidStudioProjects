package com.example.chris.budgetbuddy;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by Chris on 4/29/2017.
 *
 * I know the "create" budget is very barebones. The original plan was to have a second database that would hold the budget.
 * I probably woudl've pulled my hairout from trying to manage that. I used Sharedpreferences instead because we never had a project with them and it seemed way easier.
 * Alternatly, I thought about storing the expenses in the Sharedpreferences but It seemd trying to save an array with multiple values was both obtuse and impractical.
 *
 */

public class javacbudget  extends Activity
{
    private SQLiteAdapter muhbudget;
    private EditText edfood;
    private EditText edrent;
    private EditText edgas;
    private EditText edother;
    private TextView ttotal;
    private Button goback;
    private Button delexpend;
    private Button createbudgetgoal;
    private int curbudge;
    SharedPreferences.Editor editor;

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.createbudget);

        muhbudget = new SQLiteAdapter(this);
        SharedPreferences prefs = getSharedPreferences("muhprefs", Activity.MODE_PRIVATE);
        editor = prefs.edit();

        goback = (Button) findViewById(R.id.gb2main);
        delexpend = (Button) findViewById(R.id.delexpbutton);
        createbudgetgoal = (Button) findViewById(R.id.crtbgbutton);
        edfood = (EditText) findViewById(R.id.foodeditText2);
        edrent = (EditText) findViewById(R.id.renteditText);
        edgas = (EditText) findViewById(R.id.GaseditText3);
        edother = (EditText) findViewById(R.id.othereditText4);
        ttotal = (TextView) findViewById(R.id.curtotal);
        curbudge = prefs.getInt("bugint", 0);
        ttotal.setText("$"+curbudge);

        goback.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                finish();
            }
        });

        delexpend.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                muhbudget.openToWrite();
                muhbudget.deleteAll();
                muhbudget.close();
            }
        });

        createbudgetgoal.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {

                int food = Integer.parseInt(edfood.getText().toString());
                int rent = Integer.parseInt(edrent.getText().toString());
                int gas = Integer.parseInt(edgas.getText().toString());
                int other = Integer.parseInt(edother.getText().toString());
                int total = food + gas + rent + other;
                editor.putInt("bugint", total);
                editor.commit();
                ttotal.setText("$"+total);
            }
        });

    }
}
