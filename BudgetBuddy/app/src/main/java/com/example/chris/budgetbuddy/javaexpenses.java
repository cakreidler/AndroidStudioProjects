package com.example.chris.budgetbuddy;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

/**
 * Created by Chris on 4/29/2017.
 */

public class javaexpenses extends Activity
{

    private SQLiteAdapter muhbudget;
    private ListView listView;
    private Cursor cursor;
    private TextView total;
    private int totalint;
    private String totalstr;
    private TextView btotal;
    private int curtotal;

    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.totalexpenses);
        Button goback = (Button) findViewById(R.id.gobacktomain);
        btotal = (TextView) findViewById(R.id.textView11);
        total = (TextView) findViewById(R.id.textView5);
        listView = (ListView) findViewById(R.id.ListView1);
        SharedPreferences prefs = getSharedPreferences("muhprefs", Activity.MODE_PRIVATE);
        int curtotal = prefs.getInt("bugint", 0);
        btotal.setText("$"+curtotal);


        Intent intent = getIntent();

        muhbudget = new SQLiteAdapter(this);


        goback.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View arg0)
            {
                muhbudget.close();
                finish();

            }
        });

        muhbudget.openToRead();

        cursor = muhbudget.queueAll();
        startManagingCursor(cursor);

        String[] from = new String[]
                {
                        SQLiteAdapter.KEY_CONTENT, SQLiteAdapter.KEY_CONTENT2
                };
        int[] to = new int[]
                {
                        R.id.brand, R.id.color
                };
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.text1, cursor, from, to);

        listView.setAdapter(adapter);

        totalint = muhbudget.summer();
        totalstr = Integer.toString(totalint);
        total.setText("$"+totalstr);

        muhbudget.close();

    }




}
